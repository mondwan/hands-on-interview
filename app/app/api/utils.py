import socket
from copy import deepcopy
from typing import List

import ncclient.transport.errors
from werkzeug.exceptions import HTTPException
from flask import jsonify
from ncclient import manager
from lxml import etree


class NcclientConnectionError(Exception):
    pass


class NcclientBrokenConnectionError(Exception):
    pass


class ResponseFactory:
    @classmethod
    def createSuccessResponseDict(cls, data=None):
        ret = {'success': True}
        if data is None:
            return ret

        ret['data'] = data

        return ret

    @classmethod
    def createEmptySuccessResponseDict(cls):
        return cls.createSuccessResponseDict(None)

    @classmethod
    def createErrorResponseDict(cls, errorMessage: str):
        return {
            'success': False,
            'errorMessage': errorMessage,
        }

    @classmethod
    def createNotYetImplementResponseDict(cls):
        response = cls.createErrorResponseDict('Not yet implement')
        return response

    @classmethod
    def createNcclientConnectErrorResponseDict(cls):
        response = cls.createErrorResponseDict('Cannot connect to target')
        return response

    @classmethod
    def createNcclientConnectBrokenErrorResponseDict(cls):
        response = cls.createErrorResponseDict('Broken connection')
        return response


def generalErrorHandler(e: Exception):
    # pass through HTTP errors
    if isinstance(e, HTTPException):
        return e

    responseDict = None
    if isinstance(e, NotImplementedError):
        responseDict = ResponseFactory.createNotYetImplementResponseDict()
    elif isinstance(e, NcclientConnectionError):
        responseDict = \
            ResponseFactory.createNcclientConnectErrorResponseDict()
    elif isinstance(e, NcclientBrokenConnectionError):
        responseDict = \
            ResponseFactory.createNcclientConnectBrokenErrorResponseDict()

    if responseDict is not None:
        flaskResponse = jsonify(**responseDict)
        return flaskResponse

    raise e


class NetworkInterfaceModel:
    def __init__(self):
        self.interfaceName = None
        self.operationStatus = None
        self.statistics = {}

    def toJSON(self):
        return {
            'interfaceName': self.interfaceName,
            'operationStatus': self.operationStatus,
            'statistics': deepcopy(self.statistics),
        }


class NetworkInterfaceModelFactory:
    INTERFACE_XML_NAMESPACE = 'urn:ietf:params:xml:ns:yang:ietf-interfaces'

    @staticmethod
    def _getFirstElementFromXpath(xmle, xpath, namespaces):
        resultXml = xmle.xpath(xpath, namespaces=namespaces)[0]
        return resultXml

    def createListFromNetconfClientGetResponseRootXml(self, xmlRoot):
        xmlInterfaceXpath = '//i:interface'
        xmlNamespace = {
            'i': self.INTERFACE_XML_NAMESPACE,
        }
        xmlInterfaces = \
            xmlRoot.xpath(xmlInterfaceXpath, namespaces=xmlNamespace)

        listOfNetworkInterfaceModel = []
        for xmlInterface in xmlInterfaces:
            networkInterfaceModel = NetworkInterfaceModel()
            xmlInterfaceNameXpath = './i:name'
            xmlInterfaceName = self._getFirstElementFromXpath(
                xmlInterface, xmlInterfaceNameXpath, xmlNamespace
            )
            interfaceName = xmlInterfaceName.text
            networkInterfaceModel.interfaceName = interfaceName

            xmlInterfaceOperationStatusXpath = './i:oper-status'
            xmlOperationStatus = self._getFirstElementFromXpath(
                xmlInterface, xmlInterfaceOperationStatusXpath, xmlNamespace
            )
            operationStatus = xmlOperationStatus.text
            networkInterfaceModel.operationStatus = operationStatus

            xmlNetworkInterfaceStatisticXpath = './i:statistics'
            xmlNetworkInterfaceStatistic = self._getFirstElementFromXpath(
                xmlInterface, xmlNetworkInterfaceStatisticXpath, xmlNamespace
            )
            for xmlStat in xmlNetworkInterfaceStatistic.getchildren():
                noNamespaceXmlStat = etree.QName(xmlStat)
                statName = noNamespaceXmlStat.localname
                statValueStr = xmlStat.text
                statValueInt = int(statValueStr)
                networkInterfaceModel.statistics[statName] = statValueInt
            listOfNetworkInterfaceModel.append(networkInterfaceModel)

        return listOfNetworkInterfaceModel


class MyNcclient:
    INTERFACE_XML_NAMESPACE = 'urn:ietf:params:xml:ns:yang:ietf-interfaces'
    EXPECT_CONNECTION_ERROR_TUPLE = (
        ncclient.transport.errors.SSHError,
        socket.gaierror,
    )

    def __init__(self, host, port, username, password, logger, timeoutInSec=1):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.timeoutInSec = timeoutInSec
        self._manager = None
        self._logger = logger
        self._factory = NetworkInterfaceModelFactory()

    def connectOrRaise(self):
        try:
            self._manager = manager.connect(
                host=self.host,
                port=self.port,
                username=self.username,
                password=self.password,
                hostkey_verify=False,
                timeout=self.timeoutInSec,
            )
            isConnected = self._manager.connected
            if not isConnected:
                raise NcclientConnectionError
        except self.EXPECT_CONNECTION_ERROR_TUPLE as e:
            errorMessage = str(e)
            self._logger.error(errorMessage)
            raise NcclientConnectionError(errorMessage)

    def listAllInterfaces(self) -> List[NetworkInterfaceModel]:
        try:
            netconfResponse = self._manager.get()
        except ncclient.transport.errors.TransportError as e:
            raise NcclientBrokenConnectionError(str(e))

        xmlRoot = netconfResponse.data

        listOfNetworkInterfaceModel = \
            self._factory.createListFromNetconfClientGetResponseRootXml(
                xmlRoot
            )

        return listOfNetworkInterfaceModel
