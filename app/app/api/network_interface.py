from flask import Blueprint
from .utils import ResponseFactory, MyNcclient
from flask import request
from flask import current_app


bp = Blueprint(
    'network-interfaces', __name__, url_prefix='/network-interfaces')


def isDryRunFromRequestQueryString():
    dryRunQueryString = request.args.get('dryrun', 'true')
    isDryRun = dryRunQueryString == 'true'
    return isDryRun


def getMyNcclientFromGivenHostAndFlaskApp(host, flaskApp) -> MyNcclient:
    flaskAppConfig = flaskApp.config
    flaskLogger = flaskApp.logger
    netconfServerPort = flaskAppConfig['NETCONF_SERVER_PORT']
    netconfServerUsername = flaskAppConfig['NETCONF_SERVER_USERNAME']
    netconfServerPassword = flaskAppConfig['NETCONF_SERVER_PASSWORD']
    myNcclient = MyNcclient(
        host, netconfServerPort, netconfServerUsername,
        netconfServerPassword, flaskLogger
    )
    return myNcclient


@bp.route('/<string:host>', methods=['GET'])
def listAllNetworkInterfaces(host):
    """List all network interfaces of the given host
    """
    isDryRun = isDryRunFromRequestQueryString()
    if not isDryRun:
        myNcclient = getMyNcclientFromGivenHostAndFlaskApp(
            host, current_app
        )
        myNcclient.connectOrRaise()
        listOfNetworkInterfaceModel = myNcclient.listAllInterfaces()
        listOfJsonNetworkInterfaceModel = [
            nim.toJSON() for nim in listOfNetworkInterfaceModel
        ]
        response = ResponseFactory.createSuccessResponseDict(
            listOfJsonNetworkInterfaceModel
        )
        return response

    data = ['list', host]
    response = ResponseFactory.createSuccessResponseDict(data)
    return response


@bp.route('/<string:host>/<string:name>', methods=['POST'])
def createLoopbackInterface(host, name):
    """It creates a loopback interface with name in given host"""
    isDryRun = isDryRunFromRequestQueryString()
    if not isDryRun:
        raise NotImplementedError

    data = ['create', host, name]
    response = ResponseFactory.createSuccessResponseDict(data)
    return response


@bp.route('/<string:host>/<string:name>', methods=['DELETE'])
def deleteLoopbackInterface(host, name):
    """It deletes a loopback interface with name in given host"""
    isDryRun = isDryRunFromRequestQueryString()
    if not isDryRun:
        raise NotImplementedError

    data = ['delete', host, name]
    response = ResponseFactory.createSuccessResponseDict(data)
    return response
