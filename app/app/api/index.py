from flask import Blueprint
from .utils import ResponseFactory


bp = Blueprint('/', __name__)


@bp.route('/')
def index():
    """The index page

    For debugging purpose only, it returns an empty success response
    """
    response = ResponseFactory.createEmptySuccessResponseDict()
    return response
