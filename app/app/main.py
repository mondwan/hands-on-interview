import os

from flask import Flask
from typing import Dict
from logging import Logger

from .api.utils import generalErrorHandler
from .api.index import bp as index_bp
from .api.network_interface import bp as network_interface_bp


class DebugpyWsgiMiddleware:  # pragma: no cover
    def __init__(self, wsgiApp, flaskApp: Flask):
        self.wsgiApp = wsgiApp
        self.flaskApp = flaskApp
        self._logger = flaskApp.logger
        self._isDebugpyRunning = False

    def _setupAndRunDebugpy(self):
        debuggerListenAddress = os.getenv("DEBUGGER_LISTEN_ADDRESS", "0.0.0.0")
        try:
            debuggerListenPortStr = os.environ["DEBUGGER_LISTEN_PORT"]
            debuggerListenPortInt = int(debuggerListenPortStr)
        except (ValueError, KeyError):
            debuggerListenPortInt = 5678

        import debugpy
        import shutil

        pythonExecutablePath = shutil.which("python")
        debugpy.configure({"python": pythonExecutablePath})
        debugpy.listen((debuggerListenAddress, debuggerListenPortInt))
        self._logger.debug(
            f"Debugpy is listening on {debuggerListenAddress}:{debuggerListenPortStr} "
        )
        debugpy.wait_for_client()

    def __call__(self, *args, **kwargs):
        if not self._isDebugpyRunning:
            self._setupAndRunDebugpy()
            self._isDebugpyRunning = True

        return self.wsgiApp(*args, **kwargs)


class MyFlaskAppFactory:
    def __init__(self, testConfig):
        self.testConfig = testConfig
        self._flaskApp: Flask = None
        self._logger: Logger = None

    @staticmethod
    def _getNetconfConfigDict() -> Dict:
        netconfServerPortStr = os.environ.get("NETCONF_SERVER_PORT", "830")
        netconfConfig = {
            "NETCONF_SERVER_PORT": int(netconfServerPortStr),
            "NETCONF_SERVER_USERNAME": os.environ.get(
                "NETCONF_SERVER_USERNAME", "handsonadmin"
            ),
            "NETCONF_SERVER_PASSWORD": os.environ.get(
                "NETCONF_SERVER_PASSWORD", "handsonpassword"
            ),
        }
        return netconfConfig

    def _initializeFlaskTestConfigIfExists(self):
        if self.testConfig is not None:
            self._flaskApp.config.from_mapping(self.testConfig)

    def _initializeFlaskWithDebugpy(self):  # pragma: no cover
        isDebuggerEnabled = os.getenv("DEBUGGER_ENABLED", "false") == "true"
        if not isDebuggerEnabled:
            return

        flaskApp = self._flaskApp
        oldWsgiApp = flaskApp.wsgi_app
        newWsgiApp = DebugpyWsgiMiddleware(oldWsgiApp, flaskApp)
        self._flaskApp.wsgi_app = newWsgiApp

    def _registerFlaskBlueprints(self):
        self._flaskApp.register_blueprint(index_bp)
        self._flaskApp.register_blueprint(network_interface_bp)

    def _registerFlaskErrorHandlers(self):
        self._flaskApp.register_error_handler(Exception, generalErrorHandler)

    def buildFlaskApp(self):
        self._flaskApp = Flask(__name__)

        self._logger = self._flaskApp.logger

        netconfConfigDict = self._getNetconfConfigDict()
        self._flaskApp.config.from_mapping(netconfConfigDict)
        self._initializeFlaskTestConfigIfExists()

        self._initializeFlaskWithDebugpy()

        self._registerFlaskBlueprints()
        self._registerFlaskErrorHandlers()

        return self._flaskApp


def createFlaskApp(testConfig=None):
    factory = MyFlaskAppFactory(testConfig)
    myApp = factory.buildFlaskApp()

    return myApp


if __name__ == "__main__":
    # Only for debugging while developing
    app = createFlaskApp()
    app.run(host="0.0.0.0", port=80)
