import os

import pytest
from app.app.main import createFlaskApp
from lxml import etree


TEST_DIRECTORY_PATH = os.path.dirname(__file__)
EXAMPLE_XML_PATH = os.path.join(
    TEST_DIRECTORY_PATH,
    'netconf-client-get-response-example.xml'
)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


@pytest.fixture
def app():
    app = createFlaskApp({'TESTING': True})
    yield app


@pytest.fixture
def netconfGetResponseRootXmlFixture():
    with open(EXAMPLE_XML_PATH, 'r') as f:
        tree = etree.parse(f)

    return tree
