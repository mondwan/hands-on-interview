from app.app.api.utils import ResponseFactory
from unittest import TestCase


def test_index(client):
    expectRestResponseDict = ResponseFactory.createEmptySuccessResponseDict()
    expectStatusCode = 200
    actualFlaskResponse = client.get('/')
    actualRestResponseDict = actualFlaskResponse.get_json()
    actualStatusCode = actualFlaskResponse.status_code
    assertTc = TestCase()
    assertTc.assertEqual(expectStatusCode, actualStatusCode)
    assertTc.assertDictEqual(expectRestResponseDict, actualRestResponseDict)
