from app.app.main import createFlaskApp


def test_config():
    assert not createFlaskApp().testing
    assert createFlaskApp({'TESTING': True}).testing
