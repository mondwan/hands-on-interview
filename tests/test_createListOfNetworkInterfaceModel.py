from unittest import TestCase

from app.app.api.utils import NetworkInterfaceModelFactory


def test_createListFromNetconfClientGetResponseRootXml(
    netconfGetResponseRootXmlFixture
):
    factory = NetworkInterfaceModelFactory()
    listOfNetworkInterfaceModel = \
        factory.createListFromNetconfClientGetResponseRootXml(
            netconfGetResponseRootXmlFixture
        )

    expectNumberOfInterface = 4
    expectInterfaceNameSet = {'lo', 'tunl0', 'ip6tnl0', 'eth0'}
    actualNumberOfInterface = len(listOfNetworkInterfaceModel)
    actualInterfaceNameSet = {
        nim.interfaceName for nim in listOfNetworkInterfaceModel
    }

    assertTc = TestCase()
    assertTc.assertEqual(expectNumberOfInterface, actualNumberOfInterface)
    assertTc.assertSetEqual(expectInterfaceNameSet, actualInterfaceNameSet)
