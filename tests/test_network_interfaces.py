from app.app.api.utils import ResponseFactory
from unittest import TestCase


def test_listAllNetworkInterfaces_dryrun(client):
    expectStatusCode = 200
    expectHost = 'host1'
    endpoint = f'/network-interfaces/{expectHost}?dryrun=true'
    expectResponseData = ['list', expectHost]
    expectRestResponseDict = \
        ResponseFactory.createSuccessResponseDict(expectResponseData)

    actualFlaskResponse = client.get(endpoint)
    actualRestResponseDict = actualFlaskResponse.get_json()
    actualStatusCode = actualFlaskResponse.status_code
    assertTc = TestCase()
    assertTc.assertEqual(expectStatusCode, actualStatusCode)
    assertTc.assertDictEqual(expectRestResponseDict, actualRestResponseDict)


def test_listAllNetworkInterfaces_hostnotexists(client):
    expectStatusCode = 200
    expectHost = 'host1'
    endpoint = f'/network-interfaces/{expectHost}?dryrun=false'
    expectRestResponseDict = \
        ResponseFactory.createNcclientConnectErrorResponseDict()

    actualFlaskResponse = client.get(endpoint)
    actualRestResponseDict = actualFlaskResponse.get_json()
    actualStatusCode = actualFlaskResponse.status_code
    assertTc = TestCase()
    assertTc.assertEqual(expectStatusCode, actualStatusCode)
    assertTc.assertDictEqual(expectRestResponseDict, actualRestResponseDict)
