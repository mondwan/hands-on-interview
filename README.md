# hands-on-interview

[![pipeline status](https://gitlab.com/mondwan/hands-on-interview/badges/main/pipeline.svg)](https://gitlab.com/mondwan/hands-on-interview/-/commits/main)
[![coverage report](https://gitlab.com/mondwan/hands-on-interview/badges/main/coverage.svg)](https://mondwan.gitlab.io/hands-on-interview/coverage/)

A program, in python, listens for REST calls, and interact with a network device

```
User -----HTTP/REST----> [Your Automation Program] ---Netconf/Cli-----> Device
```

@last-modified: 2 JUL 2022

Note:

* For every input ip address, the server expects our specific client software is running on port 9988

# 1. API end points

Refer to our [openapi html](https://mondwan.gitlab.io/mondwan/hands-on-interview/openapi/)

# 2. How to start development

## 2.1 Prerequisites

* Python38
* Docker

## 2.2 Installation

```
# If venv folder does not existed at first
python3 -m venv venv

# If venv has not been activated in your terminal
. venv/bin/activate
# Your shell command line should start with (venv) afterward

pip install -r requirements-test.txt
pip install requirements.txt
```

## 2.3 Setup Vscode [optional]

Although it is an optional, I recommend you to use my vscode settings. In my setup, they
serve as Makefile liked solutions which helping you to run some long commands.

## 2.4 Launch this project with docker-compose

If you are not using vscode, check out `contrib/tasks.json` for knowing how to run commands manually

* For 1st run, you should run vscode's task `local - docker-comopse build and run`
* Otherwise, for saving time, you can run vscode's task `local - docker-comopse down and run`

### 2.4.1 Test the server

If everything runs correctly, you can curl the root url, and expect seeing a successful response

```
$ curl http://127.0.0.1:56733/
{
  "success": true
}
```

## 2.5 Running tests

* For simple pytest run, run vscode's task `local - pytest`
* For getting code coverage, run vscode's task `local - get coverage report`

## 2.6 Using vscode's debugger for debugging

* Run vscode's task `local - docker-comopse down and run with debugpy`
* Curl to the flask application for starting the debugpy's server
  * `curl http://127.0.0.1:56733/`
* You will expect to see the curl aboe stalled. In this time being, you can go `Run & Debug` in vscode
  * Select `Attach to docker container` & run it
* Happy coding with the debugger

## 3. Debug with yangcli via docker

* Assume you have run the docker development setup above

```
# Run the yangcli under host network
docker run -it --rm --net=host yuma123/yangcli:2.12
yangcli>

# Connect to the docker container in the localhost
yangcli> connect ncport=56734 server=127.0.0.1 user=handsonadmin password=handsonpassword

# Test with show vars
yangcli> show vars

# Show interfaces-state
yangcli> sget /interfaces-state

# Oneline for getting list of network interfaces
 docker run -it --rm --net=host yuma123/yangcli:2.12 \
 --ncport=56734 \
 --server=127.0.0.1 \
 --user=handsonadmin \
 --password=handsonpassword \
 --run-command="sget /interfaces-state" \
 --batch-mode \
 --timeout=5 \
 --display-mode=xml \
 --log-level=info
```

* Example xml output of list of network interfaces

```xml
<rpc-reply xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <data>
    <interfaces-state xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
      <interface>
        <name>lo</name>
        <oper-status>unknown</oper-status>
        <statistics>
          <in-octets>0</in-octets>
          <in-unicast-pkts>0</in-unicast-pkts>
          <in-errors>0</in-errors>
          <in-discards>0</in-discards>
          <in-multicast-pkts>0</in-multicast-pkts>
          <out-octets>0</out-octets>
          <out-unicast-pkts>0</out-unicast-pkts>
          <out-errors>0</out-errors>
          <out-discards>0</out-discards>
        </statistics>
      </interface>
      <interface>
        <name>tunl0</name>
        <oper-status>down</oper-status>
        <statistics>
          <in-octets>0</in-octets>
          <in-unicast-pkts>0</in-unicast-pkts>
          <in-errors>0</in-errors>
          <in-discards>0</in-discards>
          <in-multicast-pkts>0</in-multicast-pkts>
          <out-octets>0</out-octets>
          <out-unicast-pkts>0</out-unicast-pkts>
          <out-errors>0</out-errors>
          <out-discards>0</out-discards>
        </statistics>
      </interface>
      <interface>
        <name>ip6tnl0</name>
        <oper-status>down</oper-status>
        <statistics>
          <in-octets>0</in-octets>
          <in-unicast-pkts>0</in-unicast-pkts>
          <in-errors>0</in-errors>
          <in-discards>0</in-discards>
          <in-multicast-pkts>0</in-multicast-pkts>
          <out-octets>0</out-octets>
          <out-unicast-pkts>0</out-unicast-pkts>
          <out-errors>0</out-errors>
          <out-discards>0</out-discards>
        </statistics>
      </interface>
      <interface>
        <name>eth0</name>
        <oper-status>up</oper-status>
        <statistics>
          <in-octets>111371</in-octets>
          <in-unicast-pkts>738</in-unicast-pkts>
          <in-errors>0</in-errors>
          <in-discards>0</in-discards>
          <in-multicast-pkts>0</in-multicast-pkts>
          <out-octets>481471</out-octets>
          <out-unicast-pkts>664</out-unicast-pkts>
          <out-errors>0</out-errors>
          <out-discards>0</out-discards>
        </statistics>
      </interface>
    </interfaces-state>
  </data>
</rpc-reply>
```
