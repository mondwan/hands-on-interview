from setuptools import find_packages, setup

setup(
    name="app",
    version="1.0.2",
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        "flask",
    ],
)
