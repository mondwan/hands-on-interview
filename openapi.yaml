openapi: 3.0.0
info:
  title: APIs for device interactions
  description: >
    A python flask server that providing RESTful APIs for clients to interact
    with devices in netconf protocol.
  version: 1.0.2
  contact:
    name: Mond WAN
    url: https://gitlab.com/mondwan/hands-on-interview
    email: mondwan.1015@gmail.com
paths:
  /network-interfaces/{host}:
    get:
      description: It lists all network interfaces of {host}
      parameters:
        - $ref: "#/components/parameters/HostPathParam"
        - $ref: "#/components/parameters/DryrunQueryParam"
      responses:
        '200':
          description: List of interfaces of that host
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "#/components/schemas/ListOfNetworkInterfacesResponse"
                  - $ref: "#/components/schemas/DryrunResponse"
              examples:
                ListDryrunExample:
                  $ref: "#/components/examples/ListDryrunExample"
                ListNetworkInterfaceExample:
                  $ref: "#/components/examples/ListNetworkInterfaceExample"
  /network-interfaces/{host}/{name}:
    parameters:
      - $ref: "#/components/parameters/HostPathParam"
      - $ref: "#/components/parameters/NamePathParam"
      - $ref: "#/components/parameters/DryrunQueryParam"
    post:
      description: >
        It creates a loopback interface with {name} in {host} if it does not exist at the first place.
        Otherwise, it simply returns ok
      responses:
        '200':
          description: Whether it is successful to create the loopback interface or not
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "#/components/schemas/DryrunResponse"
              examples:
                CreateDryrunExample:
                  $ref: "#/components/examples/CreateDryrunExample"
    delete:
      description: >
        It deletes a loopback interface with {name} in {host} if it does exist
      responses:
        '200':
          description: Whether it is successful to delete the loopback interface or not
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "#/components/schemas/DryrunResponse"
              examples:
                CreateDryrunExample:
                  $ref: "#/components/examples/DeleteDryrunExample"
components:
  schemas:
    NetworkIntefaceStatistic:
      type: object
      required:
        - in-discards
        - in-errors
        - in-multicast-pkts
        - in-octets
        - in-unicast-pkts
        - out-discards
        - out-errors
        - out-octets
        - out-unicast-pkts
      properties:
        in-discards:
          type: number
          description: OID 1.3.6.1.2.1.2.2.1.13
        in-errors:
          type: number
          description: OID 1.3.6.1.2.1.2.2.1.14
        in-multicast-pkts:
          type: number
          description: OID 1.3.6.1.2.1.31.1.1.1.8
        in-octets:
          type: number
          description: OID 1.3.6.1.2.1.31.1.1.1.6
        in-unicast-pkts:
          type: number
          description: OID 1.3.6.1.2.1.31.1.1.1.7
        out-discards:
          type: number
          description: OID 1.3.6.1.2.1.2.2.1.19
        out-errors:
          type: number
          description: OID 1.3.6.1.2.1.2.2.1.20
        out-octets:
          type: number
          description: OID 1.3.6.1.2.1.31.1.1.1.10
        out-unicast-pkts:
          type: number
          description: OID 1.3.6.1.2.1.31.1.1.1.11
    NetworkInterface:
      type: object
      required:
        - interfaceName
        - operationStatus
        - statistics
      properties:
        interfaceName:
          type: string
          description: The name of the interface in the host
          example: eth0
        operationStatus:
          type: string
          description: The operation status of the interface
          enum:
            - up
            - down
            - unknown
        statistics:
          $ref: "#/components/schemas/NetworkIntefaceStatistic"
    StandardResponse:
      type: object
      description: Define the layout of the standard response
      required:
        - success
      properties:
        success:
          type: boolean
          description: Indicate whether the operation success or not
    DryrunResponse:
      description: The layout of the dryrun response
      allOf:
        - $ref: "#/components/schemas/StandardResponse"
        - type: object
          properties:
            data:
              type: array
              description: Describe what server will do in string
              oneOf:
                - type: string
                  description: The operation
                  enum:
                    - list
                    - create
                    - delete
                - type: string
                  description: The host argument
                - type: string
                  description: The network interface argument
              minItems: 2
              maxItems: 3
              example:
                - create
                - host1
                - interface1
    ListOfNetworkInterfacesResponse:
      description: Return list of network interfaces
      allOf:
        - $ref: "#/components/schemas/StandardResponse"
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/NetworkInterface'
  parameters:
    HostPathParam:
      in: path
      name: host
      required: true
      description: >
        The URI of target host that support interactions with netconf. Accept IPV4 in A.B.C.D format
      schema:
        type: string
    NamePathParam:
      in: path
      name: name
      required: true
      description: Tell the server which interface from that host you need
      schema:
        type: string
        example: eth0
    DryrunQueryParam:
      in: query
      name: dryrun
      required: false
      description: >
        If dryrun is true, it will returns strings about the operations server that will do
      schema:
        type: boolean
        default: true
  examples:
    ListDryrunExample:
      summary: The dry run example of list operation
      value:
        success: true
        data:
          - list
          - host1
    ListNetworkInterfaceExample:
      summary: Real example of listing network interfaces
      value:
        success: true
        data:
          - interfaceName: lo
            operationStatus: unknown
            statistics:
              in-discards: 0
              in-erros: 0
              in-multicast-pkts: 0
              in-octets: 0
              in-unicast-pkts: 0
              out-discards: 0
              out-errors: 0
              out-octets: 0
              out-unicast-pkts: 0
          - interfaceName: tunl0
            operationStatus: down
            statistics:
              in-discards: 0
              in-erros: 0
              in-multicast-pkts: 0
              in-octets: 0
              in-unicast-pkts: 0
              out-discards: 0
              out-errors: 0
              out-octets: 0
              out-unicast-pkts: 0
          - interfaceName: eth0
            operationStatus: up
            statistics:
              in-discards: 0
              in-erros: 0
              in-multicast-pkts: 0
              in-octets: 6245
              in-unicast-pkts: 42
              out-discards: 0
              out-errors: 0
              out-octets: 33921
              out-unicast-pkts: 33
    CreateDryrunExample:
      summary: The dry run example of create operation
      value:
        success: true
        data:
          - create
          - host1
          - interface1
    DeleteDryrunExample:
      summary: The dry run example of delete operation
      value:
        success: true
        data:
          - delete
          - host1
          - interface1
